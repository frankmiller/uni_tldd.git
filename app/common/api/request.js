/* ************************************************************************
 *                        _oo0oo_
 *                       o8888888o
 *                       88" . "88
 *                       (| -_- |)
 *                       0\  =  /0
 *                     ___/`---'\___
 *                   .' \\|     |// '.
 *                  / \\|||  :  |||// \
 *                 / _||||| -:- |||||- \
 *                |   | \\\  - /// |   |
 *                | \_|  ''\---/''  |_/ |
 *                \  .-\__  '-'  ___/-. /
 *              ___'. .'  /--.--\  `. .'___
 *           ."" '<  `.___\_<|>_/___.' >' "".
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /
 *      =====`-.____`.___ \_____/___.-`___.-'=====
 *                        `=---='
 * 
 * 
 ************************************************************************
 *		本代码已经过佛祖开光处理，佛祖保佑，永不宕机，永无BUG。
 *		文件名称：略
 *		简要描述：网络请求模块 
 *		作   者：Frank
 *		创建日期：2023/07/10
 *		联系方式：邮箱: goodboybbc@outlook.com, 微信：goodboybbc 可提供后台源码(Node.js开发)和数据库(MySQL)

 *		Copyright(c)	2023-2033   Frank   本项目已取得软件著作权和备案，仅供学习研究。软件著作权登记号：软著登字第12572259号 备案号:川公网安备51012202001748 蜀ICP备2024058077号-1 
 ********************************修改记录********************************
		作	者			修改日期				修改内容 
		Frank			2023/07/10			创建文件
 *************************************************************************/	
import utility from '@/common/utility.js'
import { userStore } from '@/store/modules/userStore.js'

//var userStoreObj = userStore();
export default {
	common:{
		 baseUrl:utility.baseWeb + 'api/',
		//baseUrl:"http://110.40.197.249:3000/api/",
		data:{},
		header:{
			"Content-Type":"application/json",
			"Content-Type":"application/x-www-form-urlencoded"
		},
		method:"GET",
		dataType:"json"
	},
	request( options={} ){
		if(!this.us){
			this.us = userStore();
		}
		options.url = this.common.baseUrl + options.url;
		options.data = 	options.data || this.common.data;
		options.header = options.header || this.common.header;
		options.method = options.method || this.common.method;
		options.dataType = 	options.dataType || this.common.dataType;
		
		if(options.token){
			options.data.token = this.us.persistData.token;
		}
		return new Promise((res,rej)=>{
			
			let timer = setTimeout(function () {
				uni.showLoading({
					title: '加载中'
				});
			}, 1000);
			uni.request({
				...options,
				success: (result) => {
					
					clearTimeout(timer);
					uni.hideLoading();
					
					if(result.statusCode != 200){
						return rej();
					}
					let data = result.data;
					if(data.code == 0){
						res(data);
					}else{
						rej(data.msg);
					}
				}
			})
		})
	}
}