/* ************************************************************************
 *                        _oo0oo_
 *                       o8888888o
 *                       88" . "88
 *                       (| -_- |)
 *                       0\  =  /0
 *                     ___/`---'\___
 *                   .' \\|     |// '.
 *                  / \\|||  :  |||// \
 *                 / _||||| -:- |||||- \
 *                |   | \\\  - /// |   |
 *                | \_|  ''\---/''  |_/ |
 *                \  .-\__  '-'  ___/-. /
 *              ___'. .'  /--.--\  `. .'___
 *           ."" '<  `.___\_<|>_/___.' >' "".
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /
 *      =====`-.____`.___ \_____/___.-`___.-'=====
 *                        `=---='
 * 
 * 
 ************************************************************************
 *		本代码已经过佛祖开光处理，佛祖保佑，永不宕机，永无BUG。
 *		文件名称：略
 *		简要描述：工具模块
 *		作   者：Frank
 *		创建日期：2023/07/13
 *		联系方式：邮箱: goodboybbc@outlook.com, 微信：goodboybbc 可提供后台源码(Node.js开发)和数据库(MySQL)

 *		Copyright(c)	2023-2033   Frank   本项目已取得软件著作权和备案，仅供学习研究。软件著作权登记号：软著登字第12572259号 备案号:川公网安备51012202001748 蜀ICP备2024058077号-1 
 ********************************修改记录********************************
		作	者			修改日期				修改内容 
		Frank			2023/07/13			创建文件
 *************************************************************************/	
// 工具类
export default {
	// 主域名 
	//baseWeb:'http://192.168.0.113:3000/',
	//baseWeb:'http://www.pettygame.online:3000/', 
	baseWeb:'https://www.pettygame.online:3443/', 
	//baseWeb:'http://110.40.197.249:3000/', 
	// 系统信息
	systemInfo:{},
	// 初始化，这里先获取 一些必要的信息
	init(){
		let that = this; 
		uni.getSystemInfo({
			success: (res) => {
				this.systemInfo = res;
			}
		})
	},
	// 处理请求地址，加上完整前缀
	getWebPath(url, split){
		if(!url) return undefined;
		
		// let webUrl = 'http://192.168.0.113:3000/'
		let webUrl = this.baseWeb;
		if(!split){
			return webUrl + url;
		} else{
			let urlArr = url.split(split);
			urlArr.forEach( (item, index, self) => {
				self[index] = webUrl + item;
			});
			return urlArr;
		}
	},
	
	rpxToPx(rpx) {
		return uni.upx2px(rpx);
	},
	pxToRpx(px) {
		//计算比例
		let scale = uni.upx2px(750)/750;
		return px*scale;
	},
	
	/**
	 * 转为HH：MM：SS
	 * @param second
	 * @returns {string}
	 * @private
	 */
	formatSecToHMS(second) {
		if (second < 60) {
			if (second < 10) {
				return "00:00:0" + second;
			} else {
				return "00:00:" + second;
			}
		} else {
			var min_total = Math.floor(second / 60);	// 分钟
			var sec = Math.floor(second % 60);	// 余秒
			if (min_total < 60) {
				if (min_total < 10) {
					if (sec < 10) {
						return "00:0" + min_total + ":0" + sec;
					} else {
						return "00:0" + min_total + ":" + sec;
					}
				} else {
					if (sec < 10) {
						return "00:" + min_total + ":0" + sec;
					} else {
						return "00:" + min_total + ":" + sec;
					}
				}
			} else {
				var hour_total = Math.floor(min_total / 60);	// 小时数
				if (hour_total < 10) {
					hour_total = "0" + hour_total;
				}
				var min = Math.floor(min_total % 60);	// 余分钟
				if (min < 10) {
					min = "0" + min;
				}
				if (sec < 10) {
					sec = "0" + sec;
				}
				return hour_total + ":" + min + ":" + sec;
			}
		}
	},
	
	// 时间转字符 串
	timeToString(time){
		var date = new Date();
		date.setTime(time*1000);
		
		var y = date.getFullYear(); 
		var m = date.getMonth() + 1;  
			m = m < 10 ? ('0' + m) : m;  
		var d = date.getDate();  
			d = d < 10 ? ('0' + d) : d;  
		var h = date.getHours();  
			h=h < 10 ? ('0' + h) : h;  
		var minute = date.getMinutes();  
			minute = minute < 10 ? ('0' + minute) : minute;  
		var second=date.getSeconds();  
			second=second < 10 ? ('0' + second) : second;  
		return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second; 
	}


}