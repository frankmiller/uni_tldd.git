
// #ifndef VUE3
import Vue from 'vue'
import App from './App'

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
import utility from '@/common/utility.js'
import piniaStore from '@/store'

// 初始化工具库
utility.init();

export function createApp() {
  const app = createSSRApp(App)
  app.use(piniaStore)
  return {
    app
  }
}
// #endif