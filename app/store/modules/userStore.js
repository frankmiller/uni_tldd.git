/* ************************************************************************
 *                        _oo0oo_
 *                       o8888888o
 *                       88" . "88
 *                       (| -_- |)
 *                       0\  =  /0
 *                     ___/`---'\___
 *                   .' \\|     |// '.
 *                  / \\|||  :  |||// \
 *                 / _||||| -:- |||||- \
 *                |   | \\\  - /// |   |
 *                | \_|  ''\---/''  |_/ |
 *                \  .-\__  '-'  ___/-. /
 *              ___'. .'  /--.--\  `. .'___
 *           ."" '<  `.___\_<|>_/___.' >' "".
 *          | | :  `- \`.;`\ _ /`;.`/ - ` : | |
 *          \  \ `_.   \_ __\ /__ _/   .-` /  /
 *      =====`-.____`.___ \_____/___.-`___.-'=====
 *                        `=---='
 * 
 * 
 ************************************************************************
 *		本代码已经过佛祖开光处理，佛祖保佑，永不宕机，永无BUG。
 *		文件名称：略
 *		简要描述：用户数据模块
 *		作   者：Frank
 *		创建日期：2023/07/15
 *		联系方式：邮箱: goodboybbc@outlook.com, 微信：goodboybbc 可提供后台源码(Node.js开发)和数据库(MySQL)

 *		Copyright(c)	2023-2033   Frank   本项目已取得软件著作权和备案，仅供学习研究。软件著作权登记号：软著登字第12572259号 备案号:川公网安备51012202001748 蜀ICP备2024058077号-1 
 ********************************修改记录********************************
		作	者			修改日期				修改内容 
		Frank			2023/07/15			创建文件
 *************************************************************************/	
import { defineStore } from 'pinia';
import $http from '@/common/api/request.js'
export const userStore = defineStore('user',{
	state: () => {
		return {
			user:{},
			// 持久化的数据 
			persistData:{
				token:''
			},
			// 是否登陆
			is_login:false
		}
	},
	unistorage:{
		token:'user-store-key',
		paths:['persistData']
	},
	
	actions:{
		addPath(obj, isDefault = false){
			let address = this.user.user_data.address;
			address.list.push(obj);
			if(isDefault){
				address.defaultIdx = address.list.length-1;
			}
			this.updateServerPath();
		},
		updatePath(index, obj, isDefault = false){
			let address = this.user.user_data.address;
			address.list[index] = obj;
			if(isDefault){
				if(index != address.defaultIdx){
					address.defaultIdx = index;
				}
			} else{
				if(index === address.defaultIdx){
					address.defaultIdx = -1;
				}
			}
			this.updateServerPath();
		},
		delPath(index){
			let address = this.user.user_data.address;
			if(index === address.defaultIdx){
				address.defaultIdx = -1;
			}
			address.list.splice(index, 1);
			this.updateServerPath();
		},
		updateServerPath(){
			let address = this.user.user_data.address;
			$http.request({
				url:'users/update_path',
				method:"POST",
				token:true,
				data:{
					address:JSON.stringify(address)
				}
			}).then( res => {
				this.user.user_data.address = res.data.address;
				uni.showToast({
					title:'编辑地址成功',
					icon:'none'
				})
				
			}).catch( () => {
				uni.showToast({
					title:'编辑地址失败',
					icon:'none'
				})
			});	
		},
		addCrystal(num){
			this.user.crystal += num;
		}
	},
	
	getters:{
		address(){
			return this.user.user_data.address;
		}
	}
})
	